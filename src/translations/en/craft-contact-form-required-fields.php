<?php
/**
 * craft-contact-form-required-fields plugin for Craft CMS 3.x
 *
 * Add require fields customization to the Craft CMS contact form plugin.
 *
 * @link      https://bitbucket.org/rudenyl/
 * @copyright Copyright (c) 2021 Dhens Betonio
 */

/**
 * craft-contact-form-required-fields en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('craft-contact-form-required-fields', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Dhens Betonio
 * @package   Craftcontactformrequiredfields
 * @since     1.0.0
 */
return [
    'craft-contact-form-required-fields plugin loaded' => 'craft-contact-form-required-fields plugin loaded',
];
