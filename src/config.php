<?php
/**
 * craft-contact-form-required-fields plugin for Craft CMS 3.x
 *
 * Add require fields customization to the Craft CMS contact form plugin.
 *
 * @link      https://bitbucket.org/rudenyl/
 * @copyright Copyright (c) 2021 Dhens Betonio
 */

/**
 * craft-contact-form-required-fields config.php
 *
 * This file exists only as a template for the craft-contact-form-required-fields settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'craft-contact-form-required-fields.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "someAttribute" => true,

];
