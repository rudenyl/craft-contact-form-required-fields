# craft-contact-form-required-fields plugin for Craft CMS 3.x

Add require fields customization to the Craft CMS contact form plugin.

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /craft-contact-form-required-fields

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for craft-contact-form-required-fields.

## craft-contact-form-required-fields Overview

-Insert text here-

## Configuring craft-contact-form-required-fields

-Insert text here-

## Using craft-contact-form-required-fields

-Insert text here-

## craft-contact-form-required-fields Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Dhens Betonio](https://bitbucket.org/rudenyl/)
